import { Component, ElementRef, OnInit, Pipe } from '@angular/core';
import { Plante } from '../plante';
import { AuthService } from '../services/auth.service';
import { PlanteService } from '../services/plante.service';
declare var bootstrap: any;

@Component({
  selector: 'app-plant-library',
  templateUrl: './plant-library.component.html',
  styleUrls: ['./plant-library.component.css'],
})
export class PlantLibraryComponent implements OnInit {
  formModal: any;

  activeUserIsAdmin: boolean;

  public plantes: Array<Plante>;

  public plante: Plante;
  public sendButtonIsDisable: boolean = true;
  public plantGenerated: any;
  public newPlante: Plante = new Plante();

  constructor(
    private planteService: PlanteService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.getPlants();
    // Bootstrap tooltip initialization
    let tooltipTriggerList = [].slice.call(
      document.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    let tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl);
    });

    this.activeUserIsAdmin = this.authService.getIsAdmin();
  }

  ngAfterViewInit() {}

  openFormModal() {
    this.formModal.show();
  }
  closeModal() {
    this.formModal.hide();
  }
  saveSomeThing() {
    // confirm or save something
    this.formModal.hide();
  }

  sendIsDisable() {
    console.log();

    if (
      this.newPlante.commonName !== undefined ||
      this.newPlante.commonName.length > 0 ||
      this.newPlante.latinName !== undefined ||
      this.newPlante.latinName.length > 0 ||
      this.newPlante.description !== undefined ||
      this.newPlante.description.length > 0 ||
      this.newPlante.size !== undefined ||
      this.newPlante.size > 0 ||
      this.newPlante.stock !== undefined ||
      this.newPlante.stock > 0 ||
      this.newPlante.resistance !== undefined ||
      this.newPlante.resistance > 0 ||
      this.newPlante.foliage !== undefined
    ) {
      this.sendButtonIsDisable = false;
    } else {
      this.sendButtonIsDisable = true;
    }

    console.log(this.sendButtonIsDisable);
  }

  getPlants() {
    console.log('ok get plants');
    this.planteService.getPlants().subscribe(
      (data) => {
        this.plantes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addNewPlant() {
    console.log(this.newPlante);
    this.planteService.create(this.newPlante).subscribe(
      (data) => {
        console.log(data);
      },
      (err) => {
        console.log(err);
      },
      () => {
        this.getPlants();
      }
    );
    this.getPlants();
  }
}
