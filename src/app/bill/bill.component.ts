import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css'],
})
export class BillComponent implements OnInit {
  activeOrder: Order;

  @Input() order: Order;
  constructor(private orderService: OrderService) {}

  ngOnInit() {
    this.getOrder();
  }
  getOrder() {
    this.orderService.getOrder().subscribe(
      (order) => (this.activeOrder = order),
      (err) => console.log(err),
      () => this.orderService.setOrder(null)
    );
  }
}
