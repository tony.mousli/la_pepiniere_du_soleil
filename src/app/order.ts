import { User } from './user';
import { OrderLine } from './order-line';
export class Order {
  id: number;
  user: User;
  date: string;
  version: number;
  totalPrice: number;
  orderLines: Array<OrderLine>;
}
