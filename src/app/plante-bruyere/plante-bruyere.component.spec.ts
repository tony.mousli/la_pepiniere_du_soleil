import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanteBruyereComponent } from './plante-bruyere.component';

describe('PlanteBruyereComponent', () => {
  let component: PlanteBruyereComponent;
  let fixture: ComponentFixture<PlanteBruyereComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanteBruyereComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanteBruyereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
