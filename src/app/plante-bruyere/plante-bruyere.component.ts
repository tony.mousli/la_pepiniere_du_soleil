import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plante-bruyere',
  templateUrl: './plante-bruyere.component.html',
  styleUrls: ['./plante-bruyere.component.css']
})
export class PlanteBruyereComponent implements OnInit {

  plante : Plante;
  plantes : Array<Plante>
  
  categorie = 'Terre de bruyère';
   
    constructor(private planteService : PlanteService  ) { }
  
    ngOnInit(): void {
 this.getAllPlant();    
    }
getAllPlant(){
 this.planteService.getPlanteByCategorie(this.categorie).subscribe(
        (data) => {
          this.plantes = data;
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
}
}

