import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  user: User;
  showAlert: boolean = false;
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit(): void { }

  login() {
    this.authService.login(this.username, this.password).subscribe(
      (data) => {
        this.tokenService.saveToken(data.access_token);
        this.tokenService.saveRefreshToken(data.refresh_token);
      },
      (err) => {
        console.log(err);
      },
      () => {
        this.getUser();

      }
    );
  }
  getUser() {
    this.authService.getUser().subscribe(
      (data) => {
        this.user = data;
        this.authService.setActiveUser(this.user);
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('utilisateur connecté');
        this.showAlert = true;
        setTimeout(() => { this.router.navigate(['']) }, 2500)
        // this.router.navigate(['']);
      }
    );
  }
}
