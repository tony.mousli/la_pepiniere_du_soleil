import { Pipe, PipeTransform } from '@angular/core';
import { Plante } from './plante';

@Pipe({
  name: 'plantes',
})
export class PlantesPipe implements PipeTransform {
  transform(plantes: Array<Plante>, filter: Object): Array<Plante> {
    if (!plantes || !filter) {
      return plantes;
    }
    return plantes.filter((plante) =>
      plante.commonName.includes(plante.commonName)
    );
  }
}
