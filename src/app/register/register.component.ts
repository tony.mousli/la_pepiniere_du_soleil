import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: User = new User();
  public sendButtonIsDisable: boolean = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void { }

  register() {
    console.log(this.user);
    this.authService.register(this.user).subscribe(
      (data) => {
        console.log(data);
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('utilisateur créé');
        this.router.navigate(['/login']);
      }
    );
  }

  sendIsDisable() {
    console.log();

    if (
      this.user.username !== undefined ||
      this.user.username.length > 0 ||
      this.user.password !== undefined ||
      this.user.password.length > 3 ||
      this.user.lastName !== undefined ||
      this.user.lastName.length > 0 ||
      this.user.firstName !== undefined ||
      this.user.firstName.length > 0 ||
      this.user.adresse !== undefined ||
      this.user.adresse.length > 0

    ) {
      this.sendButtonIsDisable = false;
    } else {
      this.sendButtonIsDisable = true;
    }

    console.log(this.sendButtonIsDisable);
  }
}
