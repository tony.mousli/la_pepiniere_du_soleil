import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plante-arom',
  templateUrl: './plante-arom.component.html',
  styleUrls: ['./plante-arom.component.css']
})
export class PlanteAromComponent implements OnInit {

  plante : Plante;
  plantes : Array<Plante>
  categorie = 'Plantes aromatiques';
 
    constructor(  private planteService : PlanteService  ) { }
  
    ngOnInit(): void {
      this.getAllPlantsArom();
    }
  getAllPlantsArom(){
    this.planteService.getPlanteByCategorie(this.categorie).subscribe(
      (data) => {
        this.plantes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  
  }
}
