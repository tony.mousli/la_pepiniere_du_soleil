import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanteAromComponent } from './plante-arom.component';

describe('PlanteAromComponent', () => {
  let component: PlanteAromComponent;
  let fixture: ComponentFixture<PlanteAromComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanteAromComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanteAromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
