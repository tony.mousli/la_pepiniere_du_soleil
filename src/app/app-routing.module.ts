import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlantLibraryComponent } from './plant-library/plant-library.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { AccountComponent } from './account/account.component';
import { HomeComponent } from './home/home.component';
import { PlanteExtComponent } from './plante-ext/plante-ext.component';
import { PlanteMedComponent } from './plante-med/plante-med.component';
import { PlanteBruyereComponent } from './plante-bruyere/plante-bruyere.component';
import { PlantefruitComponent } from './plantefruit/plantefruit.component';
import { PlanteAromComponent } from './plante-arom/plante-arom.component';
import { PlanteIntComponent } from './plante-int/plante-int.component';
import { BasketComponent } from './basket/basket.component';
import { BillComponent } from './bill/bill.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'planteLibrary', component: PlantLibraryComponent },

  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsNotLoggedGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [IsNotLoggedGuard],
  },
  { path: 'account', component: AccountComponent },
  {
    path: 'plantes-exterieur',
    component: PlanteExtComponent,
  },
  {
    path: 'plantes-exterieur/terre-de-bruyere',
    component: PlanteBruyereComponent,
  },
  {
    path: 'plantes-exterieur/mediterannee',
    component: PlanteMedComponent,
  },
  {
    path: 'plantes-exterieur/aromatiques',
    component: PlanteAromComponent,
  },
  {
    path: 'plantes-exterieur/fruitiers',
    component: PlantefruitComponent,
  },
  {
    path: 'plantes-interieur',
    component: PlanteIntComponent,
  },
  {
    path: 'basket',
    component: BasketComponent,
  },
  {
    path: 'bill',
    component: BillComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
