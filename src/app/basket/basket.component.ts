import { Component, OnInit } from '@angular/core';
import { OrderLine } from '../order-line';
import { BasketService } from '../services/basket.service';
import { OrderService } from '../services/order.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../user';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css'],
})
export class BasketComponent implements OnInit {
  basket: Array<OrderLine>;
  totalPrice: number;
  orderId: number;
  activeUser: User;
  constructor(
    private basketService: BasketService,
    private orderService: OrderService,
    private router: Router,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.basketInit();
    this.authService.getActiveUser().subscribe((value) => {
      this.activeUser = value;
    });
  }

  add(index) {
    this.basket[index].quantity += 1;
    this.basket[index].linePrice =
      Math.round(
        (this.basket[index].linePrice + this.basket[index].plante.price) * 100
      ) / 100;
    this.basketService.setSessionBasket(this.basket);
    this.basketInit();
  }

  sub(index) {
    if (this.basket[index].quantity > 1) {
      this.basket[index].quantity -= 1;
      this.basket[index].linePrice =
        Math.round(
          (this.basket[index].linePrice - this.basket[index].plante.price) * 100
        ) / 100;
      this.basketService.setSessionBasket(this.basket);
      this.basketInit();
    } else
      window.alert(
        "Vous ne pouvez pas réduire la quantité en dessous de 1, veuillez supprimer l'article de votre panier si vous souhaitez le retirer de votre commande"
      );
  }

  delete(index) {
    this.basket.splice(index, 1);
    this.basketService.setSessionBasket(this.basket);
    this.basketInit();
  }

  basketInit() {
    this.basket = this.basketService.parseBasket();
    let total = 0;
    this.basket.forEach((line) => {
      total += line.linePrice;
    });
    this.totalPrice = Math.round(total * 100) / 100;
  }

  emptyBasket() {
    if (
      confirm(
        'Voulez vous vraiment vider le panier ? cela supprimera toute votre sélection'
      ) == true
    ) {
      this.basketService.clearBasket();
      this.basketInit();
    } else {
    }
  }

  validateOrder() {
    if (this.activeUser != null) {
      this.orderService.create(this.totalPrice).subscribe(
        (order) => {
          this.orderId = order.id;
          console.log(order.id);
          this.orderService.setOrder(order);
        },
        (err) => {
          console.log(err);
        },
        () => {
          this.basket.forEach((line: OrderLine) => {
            this.saveOrderLine(line, this.orderId);
            this.basketService.clearBasket();
            this.basketInit;
            this.router.navigate(['/bill'], {
              relativeTo: this.activatedRoute,
            });
          });
        }
      );
    } else {
      if (
        confirm(
          "Vous n'êtes pas connecté, voulez vous accéder à la page de connexion et de création de compte ?"
        ) == true
      ) {
        this.router.navigate(['/login']);
      } else {
      }
    }
  }

  saveOrderLine(orderLine: OrderLine, orderId: number) {
    this.orderService.createOrderLine(orderLine, orderId).subscribe({
      next(orderLine) {
        console.log('ok ligne id' + orderLine.id);
      },
      error(err) {
        console.log(err);
      },
    });
  }
}
