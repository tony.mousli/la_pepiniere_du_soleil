import { Role } from './role';
export class User {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  roles: Array<Role>;
  adresse: string;
  version: number;
}
