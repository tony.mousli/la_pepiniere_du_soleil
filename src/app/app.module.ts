import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PlantLibraryComponent } from './plant-library/plant-library.component';
import { PlantCardComponent } from './plant-card/plant-card.component';
import { LoginComponent } from './login/login.component';
import { httprequestInterceptorProviders } from './httprequest.interceptor';
import { BasketComponent } from './basket/basket.component';
import { RegisterComponent } from './register/register.component';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { AccountComponent } from './account/account.component';
import { HomeComponent } from './home/home.component';
import { PlanteExtComponent } from './plante-ext/plante-ext.component';
import { PlanteMedComponent } from './plante-med/plante-med.component';
import { PlanteBruyereComponent } from './plante-bruyere/plante-bruyere.component';
import { PlanteIntComponent } from './plante-int/plante-int.component';
import { PlantefruitComponent } from './plantefruit/plantefruit.component';
import { PlanteAromComponent } from './plante-arom/plante-arom.component';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { FooterComponent } from './footer/footer.component';
import { BillComponent } from './bill/bill.component';
import { PlantesPipe } from './plantes.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PlantLibraryComponent,
    PlantCardComponent,
    LoginComponent,
    BasketComponent,
    RegisterComponent,
    AccountComponent,
    HomeComponent,
    PlanteExtComponent,
    PlanteMedComponent,
    PlanteBruyereComponent,
    PlanteIntComponent,
    PlantefruitComponent,
    PlanteAromComponent,
    FooterComponent,
    BillComponent,
    PlantesPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [httprequestInterceptorProviders, IsNotLoggedGuard, IsLoggedGuard],
  bootstrap: [AppComponent],
})
export class AppModule { }
