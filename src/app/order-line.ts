import { Order } from './order';
import { Plante } from './plante';

export class OrderLine {
  id: number;
  plante: Plante;
  quantity: number;
  linePrice: number;
  version: number;
}
