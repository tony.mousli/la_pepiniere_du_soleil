import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Plante } from '../plante';
import * as bootstrap from 'bootstrap';
import { OrderLine } from '../order-line';
import { BasketService } from '../services/basket.service';
import { PlanteService } from '../services/plante.service';
import { HttpClient } from '@angular/common/http';
import { PlantLibraryComponent } from '../plant-library/plant-library.component';
declare var bootstrap: any;
declare var window: any;

@Component({
  selector: 'app-plant-card',
  templateUrl: './plant-card.component.html',
  styleUrls: ['./plant-card.component.css'],
  providers: [PlantLibraryComponent],
})
export class PlantCardComponent implements OnInit {
  @Input() plante: Plante;
  @Input() activeUserIsAdmin: boolean;
  adminUser: boolean;
  simpleUser: boolean;
  orderLine: OrderLine = new OrderLine();
  formModal: any;
  formModalDesc: any;

  public editPlante: Plante;
  public deletePlante: Plante;
  myModal = document.getElementById('deletePlanteModal');

  constructor(
    private basketService: BasketService,
    private planteService: PlanteService
  ) {}

  ngOnInit(): void {
    console.log(this.activeUserIsAdmin);
    // Bootstrap tooltip initialization
    var tooltipTriggerList = [].slice.call(
      document.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl);
    });
  }

  addToBasket() {
    this.orderLine.plante = this.plante;
    this.orderLine.linePrice =
      Math.round(this.plante.price * this.orderLine.quantity * 100) / 100;
    this.basketService.addtoBasket(this.orderLine);
    window.alert(
      this.orderLine.quantity +
        ' ' +
        this.plante.commonName +
        ' ont été ajoutés au panier'
    );
  }

  openFormModal() {
    this.formModal.show();
  }
  openDescriptionModal() {
    this.formModalDesc.show();
  }
  closeModal() {
    this.formModal.hide();
  }
  saveSomeThing() {
    // confirm or save something
    this.formModal.hide();
  }

  deletePlant(id) {
    this.planteService.deletePlant(id).subscribe((data) => {
      console.log(data);
    });
    window.location.reload();
  }
}
