import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plante-int',
  templateUrl: './plante-int.component.html',
  styleUrls: ['./plante-int.component.css'],
})
export class PlanteIntComponent implements OnInit {
  plante: Plante;
  plantes: Array<Plante>;
  exposition = 'intérieur';

  constructor(private planteService: PlanteService) {}

  ngOnInit(): void {
    this.getAllPlantsint();
  }
  getAllPlantsint() {
    this.planteService.getPlantsByExposition(this.exposition).subscribe(
      (data) => {
        this.plantes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
