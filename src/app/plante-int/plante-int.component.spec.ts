import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanteIntComponent } from './plante-int.component';

describe('PlanteIntComponent', () => {
  let component: PlanteIntComponent;
  let fixture: ComponentFixture<PlanteIntComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanteIntComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanteIntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
