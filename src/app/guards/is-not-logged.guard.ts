import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { User } from '../user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class IsNotLoggedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  activeUser: User;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this.authService.getActiveUser().subscribe((user) => {
      this.activeUser = user;
    });
    if (this.activeUser == null) {
      return true;
    } else {
      window.alert(
        "Vous êtes déjà connecté, veuillez vous déconnecter pour accéder aux écrans de connexion et d'inscription"
      );
      return this.router.navigate(['/account']);
    }
  }
}
