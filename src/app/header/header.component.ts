import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(private authService: AuthService, public router: Router) {}
  activeUser: User = null;

  ngOnInit() {
    this.authService.getActiveUser().subscribe((value) => {
      this.activeUser = value;
    });
  }

  logout() {
    this.authService.logout();
  }
}
