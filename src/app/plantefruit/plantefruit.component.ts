import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plantefruit',
  templateUrl: './plantefruit.component.html',
  styleUrls: ['./plantefruit.component.css']
})
export class PlantefruitComponent implements OnInit {
plante : Plante;
plantes : Array<Plante>
categorie = 'Arbres fruitiers';

  constructor(private planteService : PlanteService) { }

  ngOnInit(): void {
   this.getAllPlanlsFruit() ;
  }
getAllPlanlsFruit(){
  this.planteService.getPlanteByCategorie(this.categorie).subscribe(
    (data) => {
      this.plantes = data;
    },
    (err) => {
      console.log(err);
    }
  );

}


}
