import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantefruitComponent } from './plantefruit.component';

describe('PlantefruitComponent', () => {
  let component: PlantefruitComponent;
  let fixture: ComponentFixture<PlantefruitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlantefruitComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlantefruitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
