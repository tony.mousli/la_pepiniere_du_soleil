import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../user';
import { Order } from '../order';
import { OrderLine } from '../order-line';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  activeUser: User = null;
  activeUserOrders: Array<Order> = new Array<Order>();
  constructor(
    private authService: AuthService,
    private orderService: OrderService
  ) {}

  ngOnInit(): void {
    this.authService.getActiveUser().subscribe((value) => {
      this.activeUser = value;
    });
    this.orderService.getOrdersByUser().subscribe(
      (array) => {
        this.activeUserOrders = array;
      },
      (err) => {},
      () => this.setOrderLines()
    );
  }

  setOrderLines() {
    this.activeUserOrders.forEach((order: Order) => {
      //let arrayOfOrderLines: Array<OrderLine> = new Array<OrderLine>();
      this.orderService.getOrderLinesByOrderId(order.id).subscribe((array) => {
        order.orderLines = array;
      });
    });
  }
}
