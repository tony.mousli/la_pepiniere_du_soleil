import { Component, OnInit } from '@angular/core';
import { OrderLine } from './order-line';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  panier: Array<OrderLine> = new Array<OrderLine>();
  ngOnInit(): void {
    if (sessionStorage.getItem('panier')) {
      console.log('panier déjà créé');
    } else sessionStorage.setItem('panier', JSON.stringify(this.panier));
  }
  title = 'projetFinal';
}
