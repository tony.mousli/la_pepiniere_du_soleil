import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Order } from '../order';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { OrderLine } from '../order-line';
const AUTH_API = 'http://localhost:8080/pepiniere/orders';
@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private activeOrder: BehaviorSubject<Order> = new BehaviorSubject<Order>(
    JSON.parse(sessionStorage.getItem('active-order'))
  );

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  create(totalPrice: number): Observable<Order> {
    const order: Order = new Order();
    this.authService.getActiveUser().subscribe({
      next(user) {
        order.user = user;
        order.totalPrice = totalPrice;
      },
      error(err) {
        console.log(err);
      },
      complete: () => console.log('1ère étape de commande terminée'),
    });
    return this.httpClient.post<Order>(AUTH_API, order);
  }

  createOrderLine(
    orderLine: OrderLine,
    orderId: number
  ): Observable<OrderLine> {
    return this.httpClient.post<OrderLine>(
      AUTH_API + '/' + orderId + '/line',
      orderLine
    );
  }

  setOrder(order: Order) {
    window.sessionStorage.removeItem('active-order');
    window.sessionStorage.setItem('active-order', JSON.stringify(order));
    this.activeOrder.next(JSON.parse(sessionStorage.getItem('active-order')));
  }

  getOrder(): Observable<Order> {
    return this.activeOrder;
  }

  getOrdersByUser(): Observable<Array<Order>> {
    return this.httpClient.get<Array<Order>>(AUTH_API + '/usersOrders');
  }

  getOrderLinesByOrderId(orderId: number): Observable<Array<OrderLine>> {
    return this.httpClient.get<Array<OrderLine>>(
      AUTH_API + '/details/' + orderId
    );
  }
}
