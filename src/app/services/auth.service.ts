import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../user';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { BasketService } from './basket.service';
const AUTH_API = 'http://localhost:8080/pepiniere/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
  }),
};
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private basketService: BasketService
  ) {}

  public activeUser: BehaviorSubject<User> = new BehaviorSubject(
    JSON.parse(sessionStorage.getItem('active-user'))
  );

  public isAdmin: boolean;

  login(username: string, password: string): Observable<any> {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password);
    return this.http.post(AUTH_API + 'login', body.toString(), httpOptions);
  }

  register(user: User): Observable<any> {
    const body = user;
    return this.http.post(AUTH_API + 'users', body);
  }

  getUser(): Observable<any> {
    return this.http.get(AUTH_API + 'users/user');
  }

  logout() {
    sessionStorage.removeItem('active-user');
    sessionStorage.removeItem('access-token');
    sessionStorage.removeItem('refresh-token');
    this.basketService.clearBasket();
    this.activeUser.next(null);
    this.router.navigate(['/login']);
  }

  setActiveUser(user: User) {
    window.sessionStorage.removeItem('active-user');
    window.sessionStorage.setItem('active-user', JSON.stringify(user));
    this.activeUser.next(JSON.parse(sessionStorage.getItem('active-user')));
  }

  getActiveUser(): Observable<User> {
    return this.activeUser;
  }

  getIsAdmin(): boolean {
    let roles: Array<string> = new Array<string>();
    this.getActiveUser().subscribe((user) => {
      user.roles.forEach((role) => {
        roles.push(role.name);
      });
    });
    if (roles.includes('ROLE_ADMIN')) {
      return true;
    } else return false;
  }
}
