import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor() {}

  saveToken(token: string): void {
    window.sessionStorage.removeItem('access-token');
    window.sessionStorage.setItem('access-token', token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem('access-token');
  }

  public saveRefreshToken(token: string): void {
    window.sessionStorage.removeItem('refresh-token');
    window.sessionStorage.setItem('refresh-token', token);
  }
  public getRefreshToken(): string | null {
    return window.sessionStorage.getItem('refresh-token');
  }
}
