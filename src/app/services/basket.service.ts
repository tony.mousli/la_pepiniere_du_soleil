import { Injectable } from '@angular/core';
import { Order } from '../order';
import { OrderLine } from '../order-line';

@Injectable({
  providedIn: 'root',
})
export class BasketService {
  constructor() {}

  clearBasket() {
    const panier: Array<OrderLine> = new Array<OrderLine>();
    sessionStorage.removeItem('panier');
    sessionStorage.setItem('panier', JSON.stringify(panier));
  }

  addtoBasket(orderLine: OrderLine) {
    let actualBasket: Array<OrderLine> = this.parseBasket();
    actualBasket.push(orderLine);
    this.setSessionBasket(actualBasket);
  }

  parseBasket(): Array<OrderLine> {
    return JSON.parse(sessionStorage.getItem('panier'));
  }

  setSessionBasket(basket: Array<OrderLine>) {
    sessionStorage.removeItem('panier');
    sessionStorage.setItem('panier', JSON.stringify(basket));
  }
}
