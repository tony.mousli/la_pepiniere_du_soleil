import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Plante } from '../plante';
const AUTH_API = 'http://localhost:8080/pepiniere/plantes';
@Injectable({
  providedIn: 'root',
})
export class PlanteService {
  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  deletePlant(id: number): Observable<any> {
    console.log(id);
    return this.http.delete(AUTH_API + '/' + id);
  }

  create(newPlante): Observable<any> {
    const body = newPlante;
    return this.http.post(AUTH_API, body);
  }

  getPlants(): Observable<Array<Plante>> {
    return this.http.get<Array<Plante>>(AUTH_API);
  }

  getPlanteByCategorie(categorie): Observable<Array<Plante>> {
    return this.http.get<Array<Plante>>(AUTH_API + '/category/' + categorie);
  }

  getPlantsByFloliage(foliage) {
    return this.http.get<Array<Plante>>(AUTH_API + '/lifeCycle/' + foliage);
  }
  getPlantsByExposition(exposition) {
    return this.http.get<Array<Plante>>(AUTH_API + '/exposition/' + exposition);
  }
  getPlantsByID(id): Observable<any> {
    return this.http.get({ AUTH_API } + '/' + id);
  }
}
