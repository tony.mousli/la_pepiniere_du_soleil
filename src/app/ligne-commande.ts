import { Plante } from "./plante";

export class LigneCommande {
    id : number;
    plante : Plante;
    quantite : number;

}
