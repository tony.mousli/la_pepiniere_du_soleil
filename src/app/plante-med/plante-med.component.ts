import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plante-med',
  templateUrl: './plante-med.component.html',
  styleUrls: ['./plante-med.component.css']
})
export class PlanteMedComponent implements OnInit {

  plante : Plante;
  plantes : Array<Plante>  = new Array<Plante>() ;
  categorie = 'Méditerranéennes';

    constructor(  private planteService : PlanteService  ) { }
  
    ngOnInit(): void {
      this.getAllPlanlsMed();
    }
  getAllPlanlsMed(){
    this.planteService.getPlanteByCategorie(this.categorie).subscribe(
      (data) => {
        this.plantes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  
  }
}
