import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanteMedComponent } from './plante-med.component';

describe('PlanteMedComponent', () => {
  let component: PlanteMedComponent;
  let fixture: ComponentFixture<PlanteMedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanteMedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanteMedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
