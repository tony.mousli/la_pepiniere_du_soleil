import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanteExtComponent } from './plante-ext.component';

describe('PlanteExtComponent', () => {
  let component: PlanteExtComponent;
  let fixture: ComponentFixture<PlanteExtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanteExtComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanteExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
