import { Component, OnInit } from '@angular/core';
import { Plante } from '../plante';
import { PlanteService } from '../services/plante.service';

@Component({
  selector: 'app-plante-ext',
  templateUrl: './plante-ext.component.html',
  styleUrls: ['./plante-ext.component.css'],
})
export class PlanteExtComponent implements OnInit {
  plante: Plante;
  plantes: Array<Plante> = new Array<Plante>();
  exposition = 'Soleil';

  constructor(private planteService: PlanteService) {}

  ngOnInit(): void {
    this.getAllPlanlsMed();
    this.getAllPlantsArom();
    this.getAllPlantsBruyere();
    this.getAllPlantsFuitiers();
  }
  getAllPlanlsMed() {
    this.planteService.getPlanteByCategorie('Méditerranéennes').subscribe(
      (data) => {
        this.plantes = [...this.plantes, ...data];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getAllPlantsBruyere() {
    this.planteService.getPlanteByCategorie('Terre de bruyère').subscribe(
      (data) => {
        this.plantes = [...this.plantes, ...data];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getAllPlantsFuitiers() {
    this.planteService.getPlanteByCategorie('Arbres fruitiers').subscribe(
      (data) => {
        this.plantes = [...this.plantes, ...data];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getAllPlantsArom() {
    this.planteService.getPlanteByCategorie('Plantes aromatiques').subscribe(
      (data) => {
        this.plantes = [...this.plantes, ...data];
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
