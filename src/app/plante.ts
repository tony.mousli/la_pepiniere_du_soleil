export class Plante {
  commonName: string;
  latinName: string;
  exposition: string;
  category: string;
  price: number;
  size: number;
  foliage: string;
  lifeCycle: string; // vivace / annuelle

  resistance: number;
  description: string;
  url: string;
  id: number;
  stock: number;
;

}

